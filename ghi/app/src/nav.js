function Nav() {
    return (
        <div className="container">
        <nav className="navbar sticky-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand">Conference GO!</a>
          <a className="nav-link" href="#">Home</a>
          <a className="nav-link" href="#">New location</a>
          <a className="nav-link" href="#">New conference</a>
          <a className="nav-link" href="#">New presentation</a>
        </div>
        </nav>
        </div>
    );
  }

  export default Nav;