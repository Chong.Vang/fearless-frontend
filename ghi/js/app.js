function createCard(title, description, pictureUrl, starts, ends, locName) {
    return `
      <div class="shadow-lg p-3 mb-5 bg-body-tertiary rounded card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${title}</h5>
            <h6 class="card-subtitle">${locName}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer bg-transparent border-success">
                ${dateFormatter(starts)} - ${dateFormatter(ends)}
            </div>
        </div>
      </div>
    `;
  }


function dateFormatter(datetime) {
    const date = new Date(datetime);
    const month = date.getMonth()+1;
    const day = date.getDate();
    const year = date.getFullYear();
    return `${month}/${day}/${year}`;
}

function errorHandler(alert) {
  const html = `
    <div class="alert alert-primary" role="alert">
      alert = ${alert}
    </div>`
  const error = document.querySelector('.error');
  error.innerHTML = html;
}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
          console.error("An error occurred");
          errorHandler("Response was not ok");
      } else {
        const data = await response.json();

        let count = 0;
        const columns = document.querySelectorAll('.col');
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const locName = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl, starts, ends, locName);
            const column = columns[count % columns.length];
            column.innerHTML += html;
            count += 1;
            console.log(details)
          }
        }

      }
    } catch (e) {
        console.error("Unable to grab data", e);
        errorHandler(e);
    }

  });